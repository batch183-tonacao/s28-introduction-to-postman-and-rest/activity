/*
Instructions s28 Activity:








13. Create a request via Postman to retrieve all the to do list items.
- GET HTTP method
- https://jsonplaceholder.typicode.com/todos URI endpoint
- Save this request as get all to do list items
14. Create a request via Postman to retrieve an individual to do list item.
- GET HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as get to do list item
15. Create a request via Postman to create a to do list item.
- POST HTTP method
- https://jsonplaceholder.typicode.com/todos URI endpoint
- Save this request as create to do list item
16. Create a request via Postman to update a to do list item.
- PUT HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as update to do list item PUT
- Update the to do list item to mirror the data structure used in the PUT fetch request
17. Create a request via Postman to update a to do list item.
- PATCH HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as create to do list item
- Update the to do list item to mirror the data structure of the PATCH fetch request
18. Create a request via Postman to delete a to do list item.
- DELETE HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as delete to do list item
19. Export the Postman collection and save it in the a1 folder.
20. Create a git repository named S28.
21. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
22. Add the link in Boodle.

*/

// 3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => console.log(json));


// 4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
const arrayTitle = [];
fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => {
	json.forEach(todos => arrayTitle.push(todos.title));
});
console.log(arrayTitle);


// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

fetch("https://jsonplaceholder.typicode.com/todos/8")
.then((response) => response.json())
.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}.`));


// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos",{
	method: "POST",
	headers:{
		"Content-type": "application/json"
	},
	body: JSON.stringify({
		userId: 1,
		title: "Create a to do List Item",
		completed: false
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PUT",
	headers:{
		"Content-type": "application/json"
	},
	body: JSON.stringify({
		userId: 1,
		description: "To update my to do list with different data structure.",
		title: "Updated to do list item",
		status: "pending"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// 9. Update a to do list item by changing the data structure to contain the following properties:
// - Title
// - Description
// - Status
// - Date Completed
// - User ID
// 10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
// 11. Update a to do list item by changing the status to complete and add a date when the status was changed.

fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PATCH",
	headers:{
		"Content-type": "application/json"
	},
	body: JSON.stringify({
		completed: false,
		dateCompleted: "07/09/21",
		description: "To update my to do list with different data structure.",
		status: "Complete",
		title: "Updated to do list item",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// 12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "DELETE",
})
